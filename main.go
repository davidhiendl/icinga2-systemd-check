package main

import (
	"gitlab.com/davidhiendl/icinga2-systemd-check/commands"
	_ "gitlab.com/davidhiendl/icinga2-systemd-check/commands"
)

func main() {
	commands.Execute()
}
