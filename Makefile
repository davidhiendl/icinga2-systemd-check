BINARY_PATH=./dist
BINARY_NAME=icinga2-systemd-check
BINARY_TARGET="$(BINARY_PATH)/$(BINARY_NAME)"

build:
	go build -i -ldflags="-s -w" -o "$(BINARY_TARGET)" ./main.go
	upx "$(BINARY_TARGET)"

build-dev:
	go build -i -ldflags="-s -w" -o "$(BINARY_TARGET)" ./main.go

test:
	echo "executing check ..."
	go run main.go check
