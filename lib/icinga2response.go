package lib

import (
	"fmt"
	"gitlab.com/davidhiendl/icinga2-systemd-check/lib/icinga2status"
	"os"
)

func NewResponse(message string, statusCode int) {
	println(message)
	if statusCode < icinga2status.OK || statusCode > icinga2status.UNKNOWN {
		fmt.Printf("Unknown Status Code: %v", statusCode)
		statusCode = icinga2status.UNKNOWN
	}
	os.Exit(statusCode)
}
