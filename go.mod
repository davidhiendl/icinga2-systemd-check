module gitlab.com/davidhiendl/icinga2-systemd-check

go 1.13

require (
	github.com/coreos/go-systemd/v22 v22.0.0
	github.com/spf13/cobra v0.0.6
)
