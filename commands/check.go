package commands

import (
	"fmt"
	"github.com/coreos/go-systemd/v22/dbus"
	"github.com/spf13/cobra"
	"gitlab.com/davidhiendl/icinga2-systemd-check/lib"
	"gitlab.com/davidhiendl/icinga2-systemd-check/lib/icinga2status"
)

var CheckCmd = &cobra.Command{
	Use:   "check",
	Short: "Execute a check against systemd",
	Long:  `Execute a check against systemd`,
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			fmt.Println("# Execute a check against systemd")
		}

		con, err := dbus.New()
		if err != nil {
			panic(err)
		}

		failedUnits := []dbus.UnitStatus{}
		units, err := con.ListUnits()
		for _, unit := range units {
			if unit.ActiveState != "failed" {
				continue
			}
			failedUnits = append(failedUnits, unit)
		}

		if len(failedUnits) > 0 {

			failedUnitsStr := ""
			for _, unit := range failedUnits {
				failedUnitsStr += fmt.Sprintf("%v: active=%v state=%v sub-state=%v \n", unit.Name, unit.ActiveState, unit.LoadState, unit.SubState)
			}

			lib.NewResponse("Systemd has failed units: \n"+failedUnitsStr, icinga2status.CRITICAL)
		} else {
			lib.NewResponse("Systemd has no failed units", icinga2status.OK)
		}
	},
}

func init() {
	rootCmd.AddCommand(CheckCmd)
}
