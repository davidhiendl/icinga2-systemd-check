# icinga2-systemd-check
CLI utility (in GO) for checking on systemd status using direct /run/systemd/private access for use inside a docker container with access to host sockets where systemctl fails.
